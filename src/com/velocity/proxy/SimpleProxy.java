package com.velocity.proxy;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class SimpleProxy {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		ServerSocket ss = new ServerSocket(5555);
		Socket s = ss.accept();
		Socket endp = new Socket("213.248.126.58", 5555);
			
		byte[] buffer = new byte[20000];
		int read = 0;
		InputStream endpIn = endp.getInputStream();
		InputStream clientIn = s.getInputStream();
		OutputStream endpOut = endp.getOutputStream();
		OutputStream clientOut = s.getOutputStream();
		while (true) {
			System.out.println(clientIn.available() + ", " + endpIn.available());
			if (clientIn.available() > 0) {
				System.out.println("ClientIN: " + clientIn.available());
				read = clientIn.read(buffer);
				
				endpOut.write(buffer, 0, read);
				endpOut.flush();
			}
			
			if (endpIn.available() > 0) {
				System.out.println("EndPIN: " + endpIn.available());
				read = endpIn.read(buffer);
				
				clientOut.write(buffer, 0, read);
				clientOut.flush();
				System.out.println("kk");
			}
			
			Thread.yield();
		}
	}

}
