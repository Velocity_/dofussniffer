package com.velocity.proxy;

import java.awt.TrayIcon.MessageType;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.Arrays;

public class PacketAggregator {

	public static void process(DataInputStream din, boolean fromServer, byte[] bytes, OutputStream mOutputStream) throws Exception {
		ByteBuffer bbuf = null;

		int header = (((bytes[0] & 0xFF) << 8) | (bytes[1] & 0xFF));
		int pid = header >> 2;
		int lentype = header & 3;
		int size = 0;

		byte[] len = new byte[lentype];
		if (lentype > 0) {
			din.readFully(len);

			if (lentype == 1)
				size = len[0] & 0xFF;
			else if (lentype == 2)
				size = (((len[0] & 0xFF) << 8) | (len[1] & 0xFF));
			else if (lentype == 3)
				size = ((len[0] << 16) | (len[1] << 8) | len[2]) & 0xFFFFFF;
		}

		byte[] packetData = new byte[0];
		if (size > 0) {
			packetData = new byte[size];
			//System.out.println("Trying to read " + size + "... lentype being " + lentype);
			din.readFully(packetData);
		}

		//System.out.println((fromServer ? "Incoming" : "Outgoing") + " packet. Header: " + header + ". PID: " + pid + ". Lentype: " + lentype + ". Size: " + size + ". PData: " + packetData.length);
		
		bbuf = ByteBuffer.wrap(packetData);

		boolean shouldSend = true;
		if (pid == 1) {
			System.out.println("\tProtocolRequiredMessage: {requiredVersion=" + bbuf.getInt() + ", currentVersion=" + bbuf.getInt() + "}");
		} else if (pid == 3) {
			System.out.println("\tHelloConnectMessage {}");
		} else if (pid == 4) {
			System.out.println("\tIdentificationMessage {}");
		} else if (pid == 10) {
			System.out.println("\tLoginStatusQueueMessage {position="+bbuf.getShort() + ", length=" + bbuf.getShort() + "}");
		} else if (pid == 22) {
			System.out.println("\tIdentificationSuccessMessage {}");
		} else if (pid == 182) {
			System.out.println("\tBasicPingMessage: {quiet=" + (bbuf.get() == 1) + "}");
		} else if (pid == 40) {
			System.out.println("\tServerSelectionMessage: {id=" + bbuf.getShort() + "}");
		} else if (pid == 42) {
			int serverId = bbuf.getShort() & 0xFFFF;
			String serverIP = readUTF(bbuf);
			int port = bbuf.getShort() & 0xFFFF;
			boolean canCreateNewCharacter = bbuf.get() == 1;
			String ticket = readUTF(bbuf);

			System.out.println("\tSelectedServerDataMessage {serverId=" + serverId + ", serverIP=" + serverIP + ", port=" + port + ", canCreateNewCharacter=" + canCreateNewCharacter + ", ticket=" + ticket + "}");

			byte[] fw = new byte[2 + 1 + 2 + 2 + 1 + 2 + 2 + 2 + ticket.length() + "127.0.0.1".length()];
			ByteBuffer forward = ByteBuffer.wrap(fw);
			forward.putShort((short) header);
			forward.put((byte) (9 + "127.0.0.1".length() + ticket.length()));
			forward.putShort((short) serverId);
			forward.putShort((short) "127.0.0.1".length());
			forward.put("127.0.0.1".getBytes());
			forward.putShort((short) 5554);
			forward.put(canCreateNewCharacter ? (byte) 1 : (byte) 0);
			forward.putShort((short) ticket.length());
			forward.put(ticket.getBytes());

			//DofusPacket packet = new DofusPacket(pid, fw);
			//writePacket(packet, mOutputStream);
			mOutputStream.write(fw);
			mOutputStream.flush();
			
			return;
		} else if (pid == 6112) {
			int elemId = bbuf.getInt();
			int skillId = bbuf.getShort();

			System.out.println("\tInteractiveUseEndedMessage{elemId=" + elemId + ", skillId=" + skillId + "}");
		} else if (pid == 3023) {
			int objectUID = bbuf.getInt();
			int quantity = bbuf.getInt();

			System.out.println("\tObjectQuantityMessage{objectUID=" + objectUID + ", quantity=" + quantity + "}");
		} else if (pid == 3009) {
			int weight = bbuf.getInt();
			int weightMax = bbuf.getInt();

			System.out.println("\tInventoryWeightMessage{weight=" + weight + ", weightMax=" + weightMax + "}");
		} else if (pid == 5808) {
			int entityId = bbuf.getInt();
			int value = bbuf.getInt();
			int type = bbuf.get();

			System.out.println("\tDisplayNumericalValueMessage{entityId=" + entityId + ", value=" + value + ", type=" + type + "}");
		} else if (pid == 5654) {
			int jobId = bbuf.get();
			int jobLevel = bbuf.get();
			double jobXP = bbuf.getDouble();
			double jobXpLevelFloor = bbuf.getDouble();
			double jobXpNextLevelFloor = bbuf.getDouble();

			DecimalFormat fm = new DecimalFormat("###.##");
			double lvPercent = ((jobXP - jobXpLevelFloor) / (jobXpNextLevelFloor - jobXpLevelFloor)) * 100;
			TCPForward.trayIcon.displayMessage("Dofus Assistant", "Job experience gained! Percent to level: " + fm.format(lvPercent) + "%", MessageType.INFO);

			System.out.println("\tJobExperienceUpdateMessage{jobId=" + jobId + ", jobLevel=" + jobLevel + ", jobXP=" + jobXP + ", jobXpLevelFloor=" + jobXpLevelFloor + ", jobXpNextLevelFloor=" + jobXpNextLevelFloor + "}");
		} else if (pid == 5709) {
			int elementId = bbuf.getInt();
			int elementCellId = bbuf.getShort();
			int elementState = bbuf.getInt();

			System.out.println("\tStatedElementUpdatedMessage{elementId=" + elementId + ", elementCellId=" + elementCellId + ", elementState=" + elementState + "}");
		} else if (pid == 5708) {

			System.out.println("\tInteractiveElementUpdatedMessage{}");
		} else if (pid == 5656) {
			int level = bbuf.get();

			TCPForward.trayIcon.displayMessage("Dofus Assistant", "Your Job has leveled up! New level: " + level + ".", MessageType.INFO);
		} else if (pid == 6314) {
			
			System.out.println("\tCredentialsAcknowledgementMessage {}");
		} else if (pid == 30) {
			int serverCount = bbuf.getShort();
			
			System.out.println("\tServersListMessage {");
			for (int i=0; i<serverCount; i++) {
				/*
				 * this.id=param1.readUnsignedShort();
				 * this.status=param1.readByte();
				 * this.completion=param1.readByte();
				 * this.isSelectable=param1.readBoolean();
				 * this.charactersCount=param1.readByte();
				 * this.date=param1.readDouble();
				 */
				int id = bbuf.getShort() & 0xFFFF;
				int status = bbuf.get();
				int completion = bbuf.get();
				boolean isSelectable = bbuf.get() == 1;
				int charactersCount = bbuf.get();
				double date = bbuf.getDouble();
				
				System.out.println("\t\tServerInfo {id="+id+", status="+status+", completion="+completion+", isSelectable="+isSelectable+", charactersCount="+charactersCount+", date="+date+"}");
			}
			System.out.println("\t}");
		} else if (pid == 50) {
			int id = bbuf.getShort() & 0xFFFF;
			int status = bbuf.get();
			int completion = bbuf.get();
			boolean isSelectable = bbuf.get() == 1;
			int charactersCount = bbuf.get();
			double date = bbuf.getDouble();
			
			System.out.println("\tServerStatusUpdateMessage {id="+id+", status="+status+", completion="+completion+", isSelectable="+isSelectable+", charactersCount="+charactersCount+", date="+date+"}");
		} else if (pid == 101) {
			System.out.println("\tHelloGameMessage {}");
		} else if (pid == 110) {
			System.out.println("\tAuthenticationTicketMessage {lang=\"" + readUTF(bbuf) + "\", ticket=\"" + readUTF(bbuf) + "\"}");
		} else if (pid == 6362) {
			System.out.println("\tBasicAckMessage {seq=" + bbuf.getInt() + ", lastPacketId=" + (bbuf.getShort() & 0xFFFF) + "}");
		} else if (pid == 111) {
			System.out.println("\tAuthenticationTicketAcceptedMessage {}");
		} else if (pid == 175) {
			System.out.println("\tBasicTimeMessage {timeStamp=" + bbuf.getInt() + ", timeZoneOffset=" + bbuf.getShort() + "}");
		} else if (pid == 6340) {
			System.out.println("\tServerSettingsMessage {lang=\"" + readUTF(bbuf) + "\", community=" + bbuf.get() + ", gameType=" + bbuf.get() + "}");
		} else if (pid == 6434) {
			System.out.println("\tUnknownMessage {} (COULD BE FROM NEW PROTOCOL)");
		} else if (pid == 6216) {
			System.out.println("\tAccountCapabilitiesMessage {accountId=" + bbuf.getInt() + ", tutorialAvailable=" + bbuf.get() + ", breedsVisible=" + bbuf.getShort() + ", breedsAvailable=" + bbuf.getShort() + "}");
		} else if (pid == 6305) {
			int featureCount = bbuf.getShort();
			int[] features = new int[featureCount];
			for (int i=0; i<featureCount; i++) {
				features[i] = bbuf.getShort();
			}
			System.out.println("\tServerOptionalFeaturesMessage {features=" + Arrays.toString(features) + "}");
		} else if (pid == 6267) {
			System.out.println("\tTrustStatusMessage {trusted=" + (bbuf.get() == 1) + "}");
		} else if (pid == 176) {
			System.out.println("\tBasicNoOperationMessage {}");
		} else if (pid == 150) {
			System.out.println("\tCharactersListRequestMessage {}");
		} else if (pid == 6100) {
			System.out.println("\tQueueStatusMessage {position=" + bbuf.getShort() + ", total=" + bbuf.getShort() + "}");
		} else if (pid == 189) {
			int hangUp = bbuf.get();
			int msgId = bbuf.getShort();
			String[] params = new String[bbuf.getShort()];
			for (int i=0; i<params.length; i++) params[i] = readUTF(bbuf);
			System.out.println("\tSystemMessageDisplayMessage {hangUp=" + hangUp + ", msgId=" + msgId + ", params=" + Arrays.toString(params) + "}"); //true 13 null
		} else if (pid == 151) {
			System.out.println("\tCharactersListMessage {hasStartupActions=" + (bbuf.get() == 1) + ", chars={");
			
			int count = bbuf.getShort();
			for (int c=0; c<count; c++) {
				System.out.print("\t\tCharacter(type=" + bbuf.getShort() + ") {");
				
				int id = bbuf.getInt();
				int level = bbuf.get() & 0xFF;
				String name = readUTF(bbuf);
				int bonesId = bbuf.getShort();
				int[] skins = new int[bbuf.getShort()];
				for (int i=0; i<skins.length; i++) skins[i] = bbuf.getShort();
				int[] indexedColors = new int[bbuf.getShort()];
				for (int i=0; i<indexedColors.length; i++) indexedColors[i] = bbuf.getInt();
				int[] scales = new int[bbuf.getShort()];
				for (int i=0; i<scales.length; i++) scales[i] = bbuf.getShort();
				
				int subEntitiesCount = bbuf.getShort();
				if (subEntitiesCount > 0)
					System.out.println("FUCK FUCK FUCK NOOOOOOOOOOOOOOOOOOOOO");
				
				int breed = bbuf.get();
				int sex = bbuf.get();
				
				System.out.print("id="+id+", level="+level+", name="+name+", breed=" + breed + ", sex=" + sex + ", bonesId="+bonesId+", skins="+Arrays.toString(skins) + ", indexedColors=" + Arrays.toString(indexedColors) + ", scales=" + Arrays.toString(scales));
				
				System.out.println("}");
			}
			
			System.out.println("\t}");
		} else if (pid == 5001) {
			System.out.println("\tInteractiveUseRequestMessage {elemId="+bbuf.getInt()+", skillInstanceUid="+bbuf.getInt() + "}");
		} else if (pid == 951) {
			/*
			var stepCount : uint = buffer.readUnsignedShort();
			while (stepIdx < stepCount) {
				movement = buffer.readShort();
				this.keyMovements.push(movement);
			this.actorId = buffer.readInt();
			 */
			int[] steps = new int[bbuf.getShort() & 0xFFFF];
			for (int i=0; i<steps.length; i++) steps[i] = bbuf.getShort();
			int actorId = bbuf.getInt();
			
			System.out.println("\tGameMapMovementMessage {actorId="+actorId+", steps="+Arrays.toString(steps) + "}");
		} else if (pid == 881) {
			int channel = bbuf.get();
			String content = readUTF(bbuf);
			int timestamp = bbuf.getInt();
			String fingerprint = readUTF(bbuf);
			int senderId = bbuf.getInt();
			String senderName = readUTF(bbuf);
			int senderAccountId = bbuf.getInt();
			
			System.out.println("\tChatServerMessage {channel="+channel+", content=\""+content+"\", timestamp="+timestamp+", fingerprint=\"" + fingerprint+"\", senderId="+senderId+", senderName=\"" + senderName+"\", senderAccountId="+senderAccountId+"}");
		} else if (pid == 1301) {
			int amt = bbuf.getShort();
			
			System.out.println("\tStartupActionsListMessage {");
			for (int i=0;i<amt;i++) {
				/*
				 * var _loc4_:ObjectItemInformationWithQuantity = null;
         this.uid=param1.readInt();
            this.title=param1.readUTF();
            this.text=param1.readUTF();
            this.descUrl=param1.readUTF();
            this.pictureUrl=param1.readUTF();
            _loc2_=param1.readUnsignedShort();
            _loc3_=0;
            while(_loc3_<_loc2_)
            {
               _loc4_=new ObjectItemInformationWithQuantity();
               _loc4_.deserialize(param1);
               this.items.push(_loc4_);
               _loc3_++;
            }
            return;
         }
				 */
				System.out.print("\t\tObjectAddMessage {uid="+bbuf.getInt()+", title="+readUTF(bbuf) + ", text="+readUTF(bbuf)+", descURL="+readUTF(bbuf)+", pictureURL="+readUTF(bbuf)+", items=");
				int itemcount = bbuf.getShort();
				if (itemcount > 0) {
					System.out.println(" {");
					System.out.print("\t\t\tItemInfo {objectGID="+bbuf.getShort()+", powerRate="+bbuf.getShort()+", overMax="+(bbuf.get()!=0)+", effects={");
					for (int ii=0; ii<bbuf.getShort(); ii++) {
						System.out.print(bbuf.getShort()+",");
					}
					System.out.println("}}");
					System.out.println("\t\t}");
				} else {
					System.out.println(" {}");
				}
			}
			System.out.println("\t}");
		} else {
			System.out.println((fromServer ? "Incoming packet" : "Outgoing packet") + ": " + pid + ". Length: " + size + ", available: " + bbuf.remaining());
		}
		
		if (shouldSend) {
			//DofusPacket packet = new DofusPacket(pid, packetData);
			//writePacket(packet, mOutputStream);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length + len.length + packetData.length);
			baos.write(bytes);
			baos.write(len);
			baos.write(packetData);
			mOutputStream.write(baos.toByteArray());
			//mOutputStream.flush();
		}
	}

	private static void writePacket(DofusPacket packet, OutputStream out) throws Exception {
		int lenType = 0;
		if (packet.bytes.length > 65535) {
			lenType = 3;
		}
		if (packet.bytes.length > 255) {
			lenType = 2;
		}
		if (packet.bytes.length > 0) {
			lenType = 1;
		}
		int header = (packet.id << 2) | lenType;

		out.write(header >> 8);
		out.write(header);

		if (lenType == 1)
			out.write(packet.bytes.length);
		else if (lenType == 2) {
			out.write(packet.bytes.length >> 8);
			out.write(packet.bytes.length);
		} else if (lenType == 3) {
			out.write(packet.bytes.length >> 16);
			out.write(packet.bytes.length >> 8);
			out.write(packet.bytes.length);
		} else if (lenType == 0) {
			out.flush();
			return;
		}

		out.write(packet.bytes);
		out.flush();
	}

	public static String readUTF(ByteBuffer buffer) {
		byte[] str = new byte[buffer.getShort() & 0xFFFF];
		buffer.get(str);
		return new String(str);
	}

}
