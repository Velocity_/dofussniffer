package com.velocity.proxy;

public class DofusPacket {
	
	public int id;
	public byte[] bytes;
	
	public DofusPacket(int id, byte[] bytes) {
		this.id = id;
		this.bytes = bytes;
	}

}
