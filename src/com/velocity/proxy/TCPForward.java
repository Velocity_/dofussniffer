package com.velocity.proxy;

import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;

public class TCPForward {
	public static int SOURCE_PORT = 5555;
	public static String DESTINATION_HOST = "213.248.126.39"; // 213.248.126.39
																// = lobby,
																// 213.248.126.58
																// = game
	public static int DESTINATION_PORT = 5555;
	public static TrayIcon trayIcon;

	public static void main(String[] args) throws IOException {
		boolean game = args[0].equalsIgnoreCase("game");

		if (game) {
			SOURCE_PORT = 5554;
			DESTINATION_HOST = "213.248.126.58";

			try {
				BufferedImage image = ImageIO.read(new File("ex.png"));
				trayIcon = new TrayIcon(image);
				SystemTray.getSystemTray().add(trayIcon);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		ServerSocket serverSocket = new ServerSocket(SOURCE_PORT);
		while (true) {
			Socket clientSocket = serverSocket.accept();
			ClientThread clientThread = new ClientThread(clientSocket);
			clientThread.start();
		}
	}
}

/**
 * ClientThread is responsible for starting forwarding between the client and
 * the server. It keeps track of the client and servers sockets that are both
 * closed on input/output error durinf the forwarding. The forwarding is
 * bidirectional and is performed by two ForwardThread instances.
 */
class ClientThread extends Thread {
	private Socket mClientSocket;
	private Socket mServerSocket;
	private boolean mForwardingActive = false;

	public ClientThread(Socket aClientSocket) {
		mClientSocket = aClientSocket;
	}

	/**
	 * Establishes connection to the destination server and starts bidirectional
	 * forwarding ot data between the client and the server.
	 */
	public void run() {
		InputStream clientIn;
		OutputStream clientOut;
		InputStream serverIn;
		OutputStream serverOut;
		try {
			// Connect to the destination server
			mServerSocket = new Socket(TCPForward.DESTINATION_HOST, TCPForward.DESTINATION_PORT);

			// Turn on keep-alive for both the sockets
			mServerSocket.setKeepAlive(true);
			mClientSocket.setKeepAlive(true);

			// Obtain client & server input & output streams
			clientIn = mClientSocket.getInputStream();
			clientOut = mClientSocket.getOutputStream();
			serverIn = mServerSocket.getInputStream();
			serverOut = mServerSocket.getOutputStream();
		} catch (IOException ioe) {
			System.err.println("Can not connect to " + TCPForward.DESTINATION_HOST + ":" + TCPForward.DESTINATION_PORT);
			connectionBroken();
			return;
		}

		// Start forwarding data between server and client
		mForwardingActive = true;
		ForwardThread clientForward = new ForwardThread(this, clientIn, serverOut, false);
		clientForward.start();
		ForwardThread serverForward = new ForwardThread(this, serverIn, clientOut, true);
		serverForward.start();

		System.out.println("TCP Forwarding " + mClientSocket.getInetAddress().getHostAddress() + ":" + mClientSocket.getPort() + " <--> " + mServerSocket.getInetAddress().getHostAddress() + ":" + mServerSocket.getPort() + " started.");
	}

	/**
	 * Called by some of the forwarding threads to indicate that its socket
	 * connection is brokean and both client and server sockets should be
	 * closed. Closing the client and server sockets causes all threads blocked
	 * on reading or writing to these sockets to get an exception and to finish
	 * their execution.
	 */
	public synchronized void connectionBroken() {
		try {
			mServerSocket.close();
		} catch (Exception e) {
		}
		try {
			mClientSocket.close();
		} catch (Exception e) {
		}

		if (mForwardingActive) {
			System.out.println("TCP Forwarding " + mClientSocket.getInetAddress().getHostAddress() + ":" + mClientSocket.getPort() + " <--> " + mServerSocket.getInetAddress().getHostAddress() + ":" + mServerSocket.getPort() + " stopped.");
			mForwardingActive = false;
		}
	}
}

/**
 * ForwardThread handles the TCP forwarding between a socket input stream
 * (source) and a socket output stream (dest). It reads the input stream and
 * forwards everything to the output stream. If some of the streams fails, the
 * forwarding stops and the parent is notified to close all its sockets.
 */
class ForwardThread extends Thread {
	private static final int BUFFER_SIZE = 8192;

	InputStream mInputStream;
	OutputStream mOutputStream;
	DataInputStream din;
	ClientThread mParent;
	boolean fromServer;

	/**
	 * Creates a new traffic redirection thread specifying its parent, input
	 * stream and output stream.
	 */
	public ForwardThread(ClientThread aParent, InputStream aInputStream, OutputStream aOutputStream, boolean fromServer) {
		mParent = aParent;
		mInputStream = aInputStream;
		mOutputStream = aOutputStream;
		din = new DataInputStream(mInputStream);
		this.fromServer = fromServer;
	}

	/**
	 * Runs the thread. Continuously reads the input stream and writes the read
	 * data to the output stream. If reading or writing fail, exits the thread
	 * and notifies the parent about the failure.
	 */
	public void run() {
		try {
			while (true) {
				byte[] header = new byte[2];
				din.readFully(header, 0, 2);

				PacketAggregator.process(din, fromServer, header, mOutputStream);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		mParent.connectionBroken();
	}

	public static String readUTF(ByteBuffer buffer) {
		byte[] str = new byte[buffer.getShort() & 0xFFFF];
		buffer.get(str);
		return new String(str);
	}
}